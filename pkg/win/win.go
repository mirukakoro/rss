package win

import (
	"bytes"
	_const "cdel-rss/pkg/const"
	"cdel-rss/pkg/feeds"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/url"
	"sync"
	"time"

	"github.com/Masterminds/sprig"
	"github.com/mmcdole/gofeed"

	"fyne.io/fyne/v2/layout"

	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/storage/repository"

	dialog2 "fyne.io/fyne/v2/dialog"

	"fyne.io/fyne/v2/theme"

	"fyne.io/fyne/v2/container"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
)

const tmplFallback = `
{{ .Item.Title }}
{{- if .Item.Author }} by {{ .Item.Author }}{{ end }}
{{ if .Item.UpdatedParsed }}Last Updated {{ .Item.UpdatedParsed }} ago ({{ .Item.UpdatedParsed | date "Mon Jan 2 15:04 MST 2006" }}){{ end }}
{{ if .Item.PublishedParsed }}Last Updated {{ .Item.PublishedParsed }} ago ({{ .Item.PublishedParsed | date "Mon Jan 2 15:04 MST 2006" }}){{ end }}
{{ if .Item.Categories }}
Categories:
{{ range .Item.Categories }}
- {{ . }}
{{ end }}
{{ end }}
{{ if .Item.Description }}
Description:
{{ .Item.Description }}
{{- end }}
{{ if .Item.Content }}
Content:
{{ .Item.Content }}
{{- end }}
`

//text := item.Title
//author := win.CurrentFeed.Data.Author
//if item.Author != nil {
//	author = item.Author
//}
//if author != nil {
//	text += fmt.Sprintf(" by %s", author.Name)
//}
//if item.Updated != "" {
//	text += fmt.Sprintf("\nLast Updated: %s", item.UpdatedParsed.Format("Mon Jan _2 2006"))
//}
//if item.Published != "" {
//	text += fmt.Sprintf("\nPublished: %s", item.PublishedParsed.Format("Mon Jan _2 2006"))
//}
//if len(item.Categories) != 0 {
//	text += fmt.Sprintf("\nCategories: %s", strings.Join(item.Categories, ", "))
//}
//if item.GUID != "" {
//	text += fmt.Sprintf("\nGUID: %s", item.GUID)
//}
//win.ViewItemName.SetText(text)

type Window struct {
	fyne.Window
	app fyne.App

	View         fyne.CanvasObject
	ViewItemName *widget.Label
	ViewText     *widget.Label
	ViewLink     *widget.Hyperlink
	ViewImage    *canvas.Image

	FeedSelect *widget.Select
	Feeds      feeds.Feeds

	ItemList *widget.List
	Bar      *widget.Toolbar

	CurrentFeed feeds.Feed /// current feed

	noCopy sync.Mutex
}

func (w *Window) RefreshCurrentFeed() {
	feedData := w.app.Preferences().StringWithFallback("feed", `{"name":"XKCD","link":"https://xkcd.com/atom.xml","interval":"24h"}`)
	err := json.Unmarshal([]byte(feedData), &w.CurrentFeed)
	if err != nil {
		fyne.LogError("unmarshal feed config failed", err)
	}

	err = w.CurrentFeed.Get()
	if err != nil {
		fyne.LogError("get feed failed", err)
	}
	w.ItemList.Refresh()
	w.View.Refresh()
	log.Printf("Refreshed CurrentFeed: %v items", len(w.CurrentFeed.Data.Items))
	if w.Feeds == nil {
		w.Feeds = feeds.Feeds{}
	}
	w.Feeds[w.CurrentFeed.Name] = w.CurrentFeed
	w.FeedSelect.SetSelected(w.CurrentFeed.Name)
}

func (w *Window) SyncFeedSelect() {
	var err error
	const fallback = `{"XKCD":{"name":"XKCD","link":"https://xkcd.com/atom.xml","interval":"24h"}}`
	w.Feeds, err = w.Feeds.From([]byte(w.app.Preferences().StringWithFallback("feeds", fallback)))
	if err != nil {
		fyne.LogError("failed to unmarshal feeds; default to map", err)
		w.Feeds, err = w.Feeds.From([]byte(fallback))
		if err != nil {
			fyne.LogError("failed to unmarshal feeds again; returning", err)
			return
		}
	}
	b, err := w.Feeds.To()
	if err != nil {
		fyne.LogError("failed to marshal feeds", err)
		return
	} else {
		w.app.Preferences().SetString("feeds", string(b))
	}
	opts := make([]string, 0)
	for k := range w.Feeds {
		opts = append(opts, k)
	}
	w.FeedSelect.Options = opts
}

func NewWindow(app fyne.App) *Window {
	win := &Window{
		Window: app.NewWindow("Feeds"),
		app:    app,
	}

	win.FeedSelect = widget.NewSelect(
		[]string{"Add Feed"},
		func(option string) {
			if option == "Add Feed" {
				nameEntry := widget.NewEntry()
				dialog2.NewCustomConfirm(
					"Add Feed",
					"Add",
					"Close",
					widget.NewForm(
						widget.NewFormItem("Name", nameEntry),
					),
					func(confirmed bool) {
						if confirmed {
							name := nameEntry.Text
							win.FeedSelect.Options = append(win.FeedSelect.Options, name)
							win.SyncFeedSelect()
							win.FeedSelect.SetSelected(name)
						} else {
							win.FeedSelect.ClearSelected()
						}
					},
					win,
				).Show()
				return
			}
			feedsJSON := win.app.Preferences().StringWithFallback("feeds", "[]")
			feeds_ := feeds.Feeds{}
			err := json.Unmarshal([]byte(feedsJSON), &feeds_)
			if err != nil {
				fyne.LogError("failed to unmarshal feeds", err)
				return
			}
			for _, feed := range feeds_ {
				if feed.Name == option {
					win.CurrentFeed = feed
					win.RefreshCurrentFeed()
					return
				}
			}
			fyne.LogError("failed to find feed by name (should not happen!)", nil)
		},
	)

	win.ViewItemName = widget.NewLabel("Select an item to view.")
	win.ViewText = widget.NewLabel("Select an item to view.")
	win.ViewText.Wrapping = fyne.TextWrapWord
	win.ViewImage = canvas.NewImageFromResource(theme.DownloadIcon())
	win.ViewLink = widget.NewHyperlink("Visit Link", nil)
	win.ViewLink.Hide()
	win.View = container.NewVBox(
		win.ViewText,
		win.ViewImage,
		win.ViewLink,
	)

	win.CurrentFeed = feeds.Feed{}
	win.ItemList = widget.NewList(
		func() int {
			if win.CurrentFeed.Data == nil {
				return 0
			}
			return len(win.CurrentFeed.Data.Items)
		},
		func() fyne.CanvasObject {
			return widget.NewLabel("Loading Items...")
		},
		func(id widget.ListItemID, object fyne.CanvasObject) {
			item := win.CurrentFeed.Item(id)
			text := "(error)"
			if item != nil {
				text = fmt.Sprintf("%s", item.Title)
			}
			object.(*widget.Label).SetText(text)
		},
	)
	win.ItemList.OnSelected = func(id widget.ListItemID) {
		item := win.CurrentFeed.Data.Items[id]
		buf := bytes.NewBuffer(nil)
		tmpl, err := template.New("item").Funcs(sprig.FuncMap()).Parse(win.app.Preferences().StringWithFallback("tmpl", tmplFallback))
		if tmpl == nil {
			win.ViewText.SetText(fmt.Sprintf("Error while parsing template: %s", err))
		} else {
			err = tmpl.Execute(buf, struct {
				Feed feeds.Feed
				Item *gofeed.Item
			}{
				Feed: win.CurrentFeed,
				Item: item,
			})
			if err != nil {
				win.ViewText.SetText(fmt.Sprintf("Error while executing template: %s", err))
			} else {
				win.ViewText.SetText(buf.String())
			}
		}
		win.ViewItemName.SetText(item.Title)
		uri := repository.NewFileURI("noimage")
		if item.Image != nil {
			uri = repository.NewFileURI(item.Image.URL)
		}
		win.ViewImage = canvas.NewImageFromURI(uri)
		//win.ViewText.SetText(fmt.Sprintf(
		//	"Description:\n%s\n\nContent:\n%s",
		//	item.Description,
		//	item.Content,
		//))
		err = win.ViewLink.SetURLFromString(item.Link)
		if err != nil {
			win.ViewLink.Hide()
			fyne.LogError("set hyperlink url failed, possible corrupt config?", err)
		} else {
			win.ViewLink.SetText(item.Link)
			win.ViewLink.Show()
		}
	}

	win.Bar = widget.NewToolbar(
		widget.NewToolbarAction(
			theme.InfoIcon(),
			func() {
				dialog2.NewCustom(
					"About",
					"Close",
					widget.NewLabel(_const.About),
					win,
				).Show()
			},
		),
		widget.NewToolbarAction(
			theme.ViewRefreshIcon(),
			func() {
				prog := dialog2.NewCustom("Reloading Current Feed", "Background", widget.NewProgressBarInfinite(), win)
				prog.Show()
				win.SyncFeedSelect()
				win.RefreshCurrentFeed()
				win.SyncFeedSelect()
				prog.Hide()
			},
		),
		widget.NewToolbarAction(
			theme.SettingsIcon(),
			func() {
				type PopUp widget.PopUp
				var popUp dialog2.Dialog

				// Entries
				// Name, Link, Interval
				nameEntry := widget.NewEntry()
				linkEntry := widget.NewEntry()
				linkEntry.Validator = func(link string) error {
					_, err := url.ParseRequestURI(link)
					if err2, ok := err.(*url.Error); ok {
						return err2.Err
					} else {
						return err
					}
				}
				intervalEntry := widget.NewEntry()
				intervalEntry.Validator = func(interval string) error {
					_, err := time.ParseDuration(interval)
					return err
				}
				tmplEntry := widget.NewMultiLineEntry()
				tmplEntry.Validator = func(tmpl string) error {
					_, err := template.New("validate").Funcs(sprig.FuncMap()).Parse(tmpl)
					return err
				}

				// Fill in to current settings
				nameEntry.SetText(win.CurrentFeed.Name)
				linkEntry.SetText(win.CurrentFeed.Link)
				intervalEntry.SetText(win.CurrentFeed.Interval.String())
				tmplEntry.SetText(win.app.Preferences().StringWithFallback("tmpl", tmplFallback))

				// Workaround for not being able to close modal
				popUp = dialog2.NewCustomConfirm(
					"CurrentFeed Settings",
					"Apply & Close",
					"Close",
					container.NewMax(
						widget.NewForm(
							widget.NewFormItem("CurrentFeed Name", nameEntry),
							widget.NewFormItem("CurrentFeed Link", linkEntry),
							widget.NewFormItem("Update Interval", intervalEntry),
							widget.NewFormItem("View Template", container.NewMax(tmplEntry)),
						),
					),
					func(confirmed bool) {
						if confirmed {
							if nameEntry.Validate() != nil && linkEntry.Validate() != nil && intervalEntry.Validate() != nil && tmplEntry.Validate() != nil {
								dialog2.NewError(errors.New("Validation failed"), win)
								return
							}
							prog := dialog2.NewCustom("Applying Settings", "Background", widget.NewProgressBarInfinite(), win)
							prog.Show()
							win.app.Preferences().SetString("tmpl", tmplEntry.Text)
							var err error
							win.CurrentFeed.Name = nameEntry.Text
							win.CurrentFeed.Link = linkEntry.Text
							data, err := json.Marshal(win.CurrentFeed)
							if err != nil {
								fyne.LogError("marshal feed config failed", err)
							}
							win.app.Preferences().SetString("feed", string(data))
							err = win.CurrentFeed.Get()
							win.ItemList.Refresh()
							win.RefreshCurrentFeed()
							prog.Hide()
							if err != nil {
								dialog2.ShowError(err, win)
							}
						}
					},
					win,
				)
				popUp.Show()
			},
		),
	)
	win.SetContent(container.NewBorder(
		container.NewHBox(
			//win.FeedSelect,
			win.ViewItemName,
			layout.NewSpacer(),
			win.Bar,
		),
		nil,
		win.ItemList,
		nil,
		win.View,
	))

	err := win.CurrentFeed.Get()
	if err != nil {
		fyne.LogError("initial get feed failed", err)
	}
	win.SyncFeedSelect()
	return win
}

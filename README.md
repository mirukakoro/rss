# Coloudelete Feeds

By Ken Shibata (colourdelete)

A simple Feeds reader using the Fyne UI Toolkit and github.com/mmcdole/gofeed.
Available on GitLab.com: gitlab.com/colourdelete/rss.

## Features

- View a single feed
- No images
- No HTML

Licensed under the MIT License. See the file named "LICENSE" for license information.

## TODO

- [ ] Make sure it's concurrency safe - haven't checked
- [ ] Organize code
